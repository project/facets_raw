<?php

namespace Drupal\facets_raw\Plugin\facets\widget;

use Drupal\facets\Plugin\facets\widget\ArrayWidget;
use Drupal\facets\Result\ResultInterface;

/**
 * A simple widget class that returns a rw value of the facet results.
 *
 * @FacetsWidget(
 *   id = "raw_value",
 *   label = @Translation("Raw results"),
 *   description = @Translation("A widget that builds an array with results."),
 * )
 */
class RawValue extends ArrayWidget {

  /**
   * Prepares the URL and values for the facet.
   *
   * @param \Drupal\facets\Result\ResultInterface $result
   *   A result item.
   *
   * @return array
   *   The results.
   */
  protected function prepare(ResultInterface $result) {
    return [
      'values' => $this->generateValues($result),
    ];
  }

}
